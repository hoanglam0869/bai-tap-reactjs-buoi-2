import React, { Component } from "react";

export default class Header extends Component {
  render() {
    return (
      <header
        className="text-white text-center fs-2 p-4 "
        style={{ backgroundColor: "rgba(0,0,0,0.5)" }}
      >
        TRY GLASSES APP ONLINE
      </header>
    );
  }
}
