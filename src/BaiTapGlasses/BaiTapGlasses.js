import React, { Component } from "react";
import Header from "./Header";
import { dataGlasses } from "./dataGlasses";

export default class BaiTapGlasses extends Component {
  state = {
    dataGlass: "",
  };
  handleChangeGlass = (dataGlass) => {
    this.setState({ dataGlass: dataGlass });
  };
  renderGlasses = () => {
    return dataGlasses.map((dataGlass) => (
      <div className="col-2 p-2">
        <button
          className="border border-dark"
          onClick={() => {
            this.handleChangeGlass(dataGlass);
          }}
        >
          <img className="w-100" src={dataGlass.url} />
        </button>
      </div>
    ));
  };
  render() {
    return (
      <div
        className="vh-100"
        style={{
          backgroundImage: "url(./glassesImage/background.jpg)",
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
          backgroundPosition: "center",
        }}
      >
        {/* Header */}
        <Header />
        {/* Model */}
        <div className="container py-5">
          <div className="row text-center">
            <div className="col-6 center position-relative">
              <img className="w-50" src="./glassesImage/model.jpg" />
              <div
                className="w-50 h-100 d-flex flex-column position-absolute top-0"
                style={{ transform: "translateX(47%)" }}
              >
                <div>
                  <img
                    className="w-50"
                    src={dataGlasses[6].url}
                    style={{ marginTop: "30%" }}
                  />
                </div>
                <div
                  className="text-start ms-1 me-2 p-1 mt-auto"
                  style={{ backgroundColor: "rgba(255,140,0,0.5)" }}
                >
                  <h2 className="text-primary">{dataGlasses[6].name}</h2>
                  <p className="text-white">{dataGlasses[6].desc}</p>
                </div>
              </div>
            </div>
            <div className="col-6 center position-relative">
              <img className="w-50" src="./glassesImage/model.jpg" />
              <div
                className="w-50 h-100 d-flex flex-column position-absolute top-0"
                style={{ transform: "translateX(47%)" }}
              >
                <div>
                  <img
                    className="w-50"
                    src={this.state.dataGlass.url}
                    style={{ marginTop: "30%" }}
                  />
                </div>
                <div
                  className={`text-start ms-1 me-2 p-1 mt-auto${
                    this.state.dataGlass == "" ? " d-none" : ""
                  }`}
                  style={{ backgroundColor: "rgba(255,140,0,0.5)" }}
                >
                  <h2 className="text-primary">{this.state.dataGlass.name}</h2>
                  <p className="text-white">{this.state.dataGlass.desc}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* Glasses */}
        <div className="container bg-white py-5">
          <div className="row px-5">{this.renderGlasses()}</div>
        </div>
      </div>
    );
  }
}
